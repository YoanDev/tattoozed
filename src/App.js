import './App.css';
import Home from './pages';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax';


function App() {
  return (
    <>
    <Router>

      <ParallaxProvider>
        <Parallax strength={500}>

        <Home/>

        </Parallax>
        

      </ParallaxProvider>

      
    </Router>
      
    </>
  );
}

export default App;
