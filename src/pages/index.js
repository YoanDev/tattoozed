import React from 'react'
import Footer from '../components/Footer'
import HeroSection from '../components/HeroSection'
import InfoSection from '../components/InfoSection'
import { homeObjOne } from '../components/InfoSection/Data'
import Navbar from '../components/Navbar'
import Offers from '../components/Offers'
import Sidebar from '../components/Sidebar'
import TatooSection from '../components/TatooSection'
import TeamSection from '../components/TeamSection'
import {useState} from 'react'


const Home = () => {
    const [isOpen, setIsOpen] = useState(false)

    const toggle = ()=>{
        setIsOpen(!isOpen)
    }
    return (
        <div>
            <Navbar toggle={toggle} />
            <Sidebar isOpen={isOpen} toggle={toggle} />
            <HeroSection />
            <InfoSection {...homeObjOne} />
            <TatooSection />
            <Offers />
            <TeamSection />
            <Footer/>
        </div>
    )
}

export default Home
