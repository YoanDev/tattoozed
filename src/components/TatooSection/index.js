import React from 'react'
import Icon1 from '../images/one-arm-tatoo.jpeg'
import Icon2 from '../images/leg-full-tatoo-01.jpeg'
import Icon3 from '../images/back-tatoo.jpeg'
import Icon4 from '../images/forearm-tatoo.jpg'
import Icon5 from '../images/fullbody-tatoos.jpg'
import Icon6 from '../images/ankle-tatoo.jpeg'
import { ServicesCard, ServicesContainer, ServicesH1, ServicesH2, ServicesIcon, ServicesP, ServicesWrapper } from './TatooElement.jsx'


const TatooSection = () => {
    return (
        <ServicesContainer id='tatoos'>
            <ServicesH1>Tatoos</ServicesH1>
            <ServicesWrapper>
                <ServicesCard>
                    <ServicesIcon src={Icon1}/>
                    <ServicesH2>Arm tatoos</ServicesH2>
                    <ServicesP>D</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon2}/>
                    <ServicesH2>Leg Tatoos</ServicesH2>
                    <ServicesP>A</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon3}/>
                    <ServicesH2>Back tatoos</ServicesH2>
                    <ServicesP>S</ServicesP>
                </ServicesCard>

                <ServicesCard>
                    <ServicesIcon src={Icon4}/>
                    <ServicesH2>Forearms Tatoos</ServicesH2>
                    <ServicesP>F2</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon5}/>
                    <ServicesH2>Neck Tatoos</ServicesH2>
                    <ServicesP>N</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon6}/>
                    <ServicesH2>Ankle Tatoos</ServicesH2>
                    <ServicesP>ANK</ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default TatooSection
