import styled from 'styled-components'


export const ServicesContainer = styled.div`
    height:1000px;
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    background: #010606;
    position: absolute;
    top:150em;
    z-index:10;
    box-shadow: 10px 10px 5px  grey;


    @media screen and (max-width: 768px){
        height:1100px;
        top:200em;
    }
    @media screen and (max-width: 480px){
        height:1300px;
        top:170em;
    }
`


export const ServicesWrapper = styled.div`
    max-width:1000px;
    display:grid;
    grid-template-columns:1fr 1fr 1fr;
    align-items: center;
    grid-gap:46px;
    padding:0 50px;



    @media screen and (max-width: 1000px){
        grid-template-columns: 1fr 1fr;
    }
    @media screen and (max-width: 768px){
        grid-template-columns: 1fr;
        padding: 0 20px
    }
`


export const ServicesCard = styled.div`
    background: #ffffff;
    display:flex;
    flex-direction: column;
    justify-content:flex-start;
    align-items:center;
    border-radius:10px;
    max-height:340px;
    padding:30px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.2);
    
    transition: all 0.1s ease-in-out;

    &:before{
        content:'';
        border-right:10px;
        border-bottom-right-radius:10px;
        position: absolute;
        top:0;
        left:150px;
        right:0;
        bottom:0;
        background: linear-gradient(180deg, rgba(0,0,0,0.2) 0%,rgba(0,0,0,0.6)100%), linear-gradient(180deg, rgba(0,0,0,0.2) 0%, transparent 100%);
        z-index:-20;
    }

    &:hover{
        transform:scale(1.02);
        transition: all 0.2s ease-in-out;
        cursor: pointer;
    }
`


export const ServicesIcon = styled.img`
    height:280px;
    width:250px;
    margin-bottom: 10px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

`


export const ServicesH1 = styled.h1`
    font-size:2.5rem;
    color:#fff;
    margin-bottom:64px;


    @media screen and (max-width: 480px){
        font-size:2rem;
    }
`
export const ServicesH2 = styled.h2`
    font-size:1rem;
    text-align:center;
    color:#010606;
`

export const ServicesP = styled.p`
    font-size:1rem;
    text-align:center;
`