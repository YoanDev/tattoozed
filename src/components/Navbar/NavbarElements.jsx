import styled from 'styled-components'
import { Link as LinkR } from 'react-router-dom'
import { Link as LinkS } from 'react-scroll'


export const Nav = styled.nav`
 background:rgba(243, 245, 242, 0.959);
 display:flex;
 position:sticky;
 width:100%;
 height:120px;
 justify-content:center;
 align-items:center;
 top:0;
 z-index:100;
 font-size:22px;
 box-shadow: 0 3px 2px -2px #f8f8f8;
`

export const NavbarContainer = styled.div`
 display:flex;
 justify-content:space-between;
 height:80px;
 width:100%;
 padding:0 24px;
 max-width:1100px;
`



export const NavTatoo = styled.div`
 width:100px;
 position: relative;
 display:flex;
 align-items:center;
 justify-content:center;
 height:100px;
 left:0;
 top:-10px;
 
` 


export const MobileIcon = styled.div`
 display:none;
 color:#000;


 @media screen and (max-width:768px){
     display:block;
     position:absolute;
     top:20px;
     right:0;
     font-size:2rem;
     transform:translate(-100%, 60%);
     cursor: pointer;
 }
`

export const NavMenu = styled.ul`
 display:flex;
 align-items:center;
 text-align:center;
 list-style:none;
 margin-right:-22px;

 @media screen and (max-width:768px){
     display:none;
 }
`

export const NavItems = styled.li`
 height:80px;
`

export const NavLinks = styled(LinkS)`
    color:#000;
    display:flex;
    align-items:center;
    text-decoration:none;
    padding:0 2rem;
    height:100%;
    cursor: pointer;

    &.active{
        border-bottom: 3px solid #010606;
    }
`

export const NavBtn = styled.nav`
    display:flex;
    align-items:center;


    @media screen and (max-width:768px){
        display:none;
    }
`


export const NavBtnLink = styled(LinkR)`
    border-radius:50px;
    background: #010606;
    white-space: nowrap;
    padding: 10px 22px;
    color:#fff;
    font-size:16px;
    outline:none;
    border: none;
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    text-decoration:none;

    &:hover {
    transition: all 0.2s ease-in-out;
    background:#fff;
    color:#010606;
    }
`