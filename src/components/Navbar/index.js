import React from 'react'
import { MobileIcon, Nav, NavbarContainer, NavBtn, NavBtnLink, NavItems, NavLinks, NavMenu, NavTatoo } from './NavbarElements'
import {FaBars} from 'react-icons/fa'
import Navbg from '../images/dragon-tatoo.png'
import './Navbar.css'

const Navbar = ({toggle}) => {
    

    return (
        <>
        <Nav>
            <NavbarContainer>
                <NavTatoo >
                <img src={ Navbg } className='nav-tatoo' alt="nav-img"/>
                </NavTatoo>
                <MobileIcon onClick={toggle}>
                    <FaBars/>
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='about'smooth={true} duration={500} spy={true} exact='true' offset={-80}>About</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='tatoos' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Tatoos</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='offers' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Offers</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='team' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Team</NavLinks>
                    </NavItems>
                    
                    
                        
                </NavMenu>

                <NavBtn>
                    <NavBtnLink to='/signin'>Sign In</NavBtnLink>
                </NavBtn>
            </NavbarContainer>
        </Nav>
        </>
    )
}

export default Navbar
