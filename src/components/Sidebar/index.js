import React from 'react'
import {CloseIcon, Icon, SidebarContainer, SidebarRoute, SideBtnWrap, SidebarWrapper, SidebarLink, SidebarMenu} from './SidebarElements.jsx'

const Sidebar = ({isOpen, toggle}) => {
    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
            <Icon onClick={toggle}>
                <CloseIcon />
            </Icon>
            <SidebarWrapper>
                <SidebarMenu>
                    <SidebarLink  to='about' onClick={toggle}>
                        About
                    </SidebarLink>
                    <SidebarLink to='tatoos' onClick={toggle}>
                        Tatoos
                    </SidebarLink> 
                    <SidebarLink to='offers' onClick={toggle}>
                        Offers
                    </SidebarLink>
                    <SidebarLink to='team' onClick={toggle}>
                        Team
                    </SidebarLink>
                </SidebarMenu>
                <SideBtnWrap>
                    <SidebarRoute to='/signin' >Sign In</SidebarRoute>
                </SideBtnWrap>
            </SidebarWrapper>
        </SidebarContainer>
    )
}

export default Sidebar
