import React, { useState, useEffect } from 'react'
import { ArrowForward, ArrowRight, HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, VideoBg, HeroBtnWrapper } from '././HeroElements.jsx'
import Tatoo01 from '../images/tatoo-01.png'
import { Parallax } from 'react-scroll-parallax';
import './HeroSection.css'

const HeroSection = () => {
    const [hover, setHover] = useState(false)

    const onHover = () => {
        setHover(!hover)
    }

    const [offsetY, setOffsetY] = useState(0)
    const handleScroll = () => setOffsetY(window.pageYOffset)

    useEffect(()=>{
        window.addEventListener("scroll", handleScroll)

        return () => window.removeEventListener('scroll', handleScroll)
    },[])
    return (
        <HeroContainer>
            <HeroBg>
                
            </HeroBg>
            <HeroContent>
                <HeroH1>Tattoozed</HeroH1>
                <HeroP>
                    The best quality for your skin
                </HeroP>

                   
                    
                    <img style={{transform: `translateY(${-offsetY * 0.5}px)`}} className="tatoo01" src={ Tatoo01 } alt=""/>
                    <img style={{transform: `translateY(${offsetY * 0.5}px)`}} className="tatoo02" src={ Tatoo01 } alt=""/>

                    <img style={{transform: `translateY(${-offsetY * 0.5}px)`}} className="tatoo03" src={ Tatoo01 } alt=""/>
                    <img style={{transform: `translateY(${offsetY * 0.5}px)`}} className="tatoo04" src={ Tatoo01 } alt=""/>

                    <img style={{transform: `translateY(${-offsetY * 0.5}px)`}} className="tatoo05" src={ Tatoo01 } alt=""/>


            
            </HeroContent>
            
            

        </HeroContainer>
        
    )
}

export default HeroSection
