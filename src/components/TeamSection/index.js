import React from 'react'
import Icon1 from '../images/team-member1.jpg'
import Icon2 from '../images/team-member2.jpg'
import Icon3 from '../images/team-member3.jpeg'


import { ServicesCard, ServicesContainer, ServicesH1, ServicesH2, ServicesIcon, ServicesP, ServicesWrapper } from './TeamElements'


const TeamSection = () => {
    return (
        <ServicesContainer id='team'>
            <ServicesH1>Team</ServicesH1>
            <ServicesWrapper>
                <ServicesCard>
                    <ServicesIcon src={Icon1}/>
                    <ServicesH2>Josh</ServicesH2>
                    <ServicesP>Leader</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon2}/>
                    <ServicesH2>Zoe</ServicesH2>
                    <ServicesP>5 years experiences</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon3}/>
                    <ServicesH2>Mikey</ServicesH2>
                    <ServicesP>The best</ServicesP>
                </ServicesCard>

               
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default TeamSection
